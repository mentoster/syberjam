namespace Documents
{
    public enum DocumentFinalizePlace
    {
        Folder,
        Garbage,
        Shredder
    }
}