using System;
using UnityEngine;

namespace Documents
{
    [Serializable]
    public struct DocumentData
    {
        public Color[] colors;
        public DocumentFinalizePlace documentFinalizePlace;
        public DocumentRequirement toDo;
        public DocumentRequirement done;
    }
}