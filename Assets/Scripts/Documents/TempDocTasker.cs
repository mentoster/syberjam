using System;
using UnityEngine;

namespace Documents
{
    public class TempDocTasker : MonoBehaviour
    {
        [SerializeField] private int add;
        [SerializeField] private DocumentsGenerator generator;
        [SerializeField] private DocumentsController controller;

        [ContextMenu("Test")]
        private void FillController()
        {
            generator.FillWithDocuments(add);
        }

        private void Start()
        {
            controller.onDocumentFinalized += ControllerOnDocumentFinalized;
        }

        private void ControllerOnDocumentFinalized(int score)
        {
            Debug.unityLogger.Log("TempDocTasker.ControllerOnDocumentFinalized", score);
        }
    }
}