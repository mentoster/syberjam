using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Documents
{
    public class DocumentRequirementHolder: MonoBehaviour
    {
        [SerializeField] private DocumentRequirement requirement;
        public event Action<DocumentRequirement> onRequirementClick;


        private void OnMouseDown()
        {
            onRequirementClick?.Invoke(requirement);
        }
    }
}