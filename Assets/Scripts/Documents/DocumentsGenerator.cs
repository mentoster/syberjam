using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Documents
{
    public class DocumentsGenerator: MonoBehaviour
    {
        [SerializeField] private float delay;
        [SerializeField] private int maxLines;

        [SerializeField] private DocumentsController controller;
        public List<Rule> rules;

        private DocumentData CreateDocument()
        {
            DocumentData data = new DocumentData();

            var rulesToApply = rules.
                Where(x => x is SimpleRule).
                OrderBy(x => Random.value).
                Take(Random.Range(0, rules.Count + 1)).
                ToArray();

            int linesCount = Random.Range((int)(maxLines / 2), maxLines);

            int[] indexes = new int[linesCount];

            for (int i = 0; i < linesCount; i++)
                indexes[i] = i;

            indexes = indexes.OrderBy(x => Random.value).Take(rulesToApply.Length).ToArray();

            data.colors = new Color[linesCount];
            for (int i = 0; i < linesCount; i++)
            {
                data.colors[i] = Color.gray;
            }

            for (int i = 0; i < rulesToApply.Length; i++)
            {
                if (rulesToApply[i] is SimpleRule simpleRule)
                {
                    data.toDo |= simpleRule.requirement;
                    data.colors[indexes[i]] = simpleRule.color;
                }
            }

            return data;
        }

        private IEnumerator FillDocuments(int i)
        {
            for (int j = 0; j < i; j++)
            {
                controller.AddDocuments(new []{CreateDocument()});
                yield return new WaitForSeconds(delay);
            }
        }

        public void FillWithDocuments(int count)
        {
            StartCoroutine(FillDocuments(count));
        }
    }
}