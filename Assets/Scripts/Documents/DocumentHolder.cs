using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Documents
{
    public class DocumentHolder: MonoBehaviour, IDragHandler
    {
        [SerializeField] private Image imagePref;
        [SerializeField] private Transform canvas;
        private Camera _camera;
        public DocumentData documentData;

        public void DisplayDocument(DocumentData data)
        {
            documentData = data;
            foreach (Color color in data.colors)
            {
                Instantiate(imagePref, canvas).color = color;
            }
        }

        private void Start()
        {
            _camera = Camera.main;
        }


        public void OnDrag(PointerEventData eventData)
        {
            Vector3 to = _camera.ScreenToWorldPoint(eventData.position);
            Vector3 from = _camera.ScreenToWorldPoint(eventData.position - eventData.delta);
            transform.position += to - from;
        }
    }
}