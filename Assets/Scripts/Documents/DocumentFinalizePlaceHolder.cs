using System;
using UnityEngine;

namespace Documents
{
    public class DocumentFinalizePlaceHolder: MonoBehaviour
    {
        [SerializeField] private DocumentFinalizePlace finalizePlace;
        public event Action<DocumentFinalizePlace, DocumentData> onDocumentFinalized;

        private void OnTriggerEnter2D(Collider2D col)
        {
            DocumentHolder holder = col.GetComponent<DocumentHolder>();
            if(!holder)
                return;
            
            onDocumentFinalized?.Invoke(finalizePlace, holder.documentData);
            Destroy(holder.gameObject);
        }
    }
}