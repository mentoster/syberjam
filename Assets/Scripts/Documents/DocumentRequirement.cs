using System;

namespace Documents
{
    [Flags]
    public enum DocumentRequirement
    {
        Press = 1,
        Sharp = 2,
        Sign = 4,
        DoNotUse = 128
    }
}