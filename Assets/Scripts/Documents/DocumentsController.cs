using System;
using Unity.Mathematics;
using UnityEngine;

namespace Documents
{
    public class DocumentsController: MonoBehaviour
    {
        [SerializeField] private DocumentsStack documentsStack;
        [SerializeField] private DocumentRequirementHolder[] holders;
        [SerializeField] private DocumentFinalizePlaceHolder[] finalizers;
        [SerializeField] private DocumentHolder documentPrefab;
        [Space] 
        [SerializeField] private Transform newDocumentPos;


        private DocumentHolder _currentDocumentHolder;

        public event Action<int> onDocumentFinalized; 

        private void Start()
        {
            documentsStack.onDocumentTaken += DocumentsStackOnDocumentTaken;

            foreach (DocumentRequirementHolder documentRequirementHolder in holders)
                documentRequirementHolder.onRequirementClick += DocumentRequirementHolderOnRequirementClick;

            foreach (DocumentFinalizePlaceHolder documentFinalizePlace in finalizers)
                documentFinalizePlace.onDocumentFinalized += DocumentFinalizePlaceOnDocumentFinalized;
        }

        private void DocumentFinalizePlaceOnDocumentFinalized(DocumentFinalizePlace finalizePlace, DocumentData data)
        {
            documentsStack.onDocumentTaken += DocumentsStackOnDocumentTaken;

            int result = CompareDocsRequirement(data);
            result += finalizePlace != data.documentFinalizePlace ? 2 : 0;
            onDocumentFinalized?.Invoke(result);
            
        }

        private void DocumentRequirementHolderOnRequirementClick(DocumentRequirement obj)
        {
            if(_currentDocumentHolder == null)
                return;
            
            DocumentData data = _currentDocumentHolder.documentData;
            data.done |= obj;
            _currentDocumentHolder.documentData = data;
        }

        private void DocumentsStackOnDocumentTaken(DocumentData documentData)
        {
            documentsStack.onDocumentTaken -= DocumentsStackOnDocumentTaken;

            _currentDocumentHolder =
                Instantiate(documentPrefab, newDocumentPos.transform.position, quaternion.identity);
            
            _currentDocumentHolder.DisplayDocument(documentData);
        }

        private void OnDestroy()
        {
            documentsStack.onDocumentTaken -= DocumentsStackOnDocumentTaken;

            foreach (DocumentRequirementHolder documentRequirementHolder in holders)
                documentRequirementHolder.onRequirementClick -= DocumentRequirementHolderOnRequirementClick;

            foreach (DocumentFinalizePlaceHolder documentFinalizePlace in finalizers)
                documentFinalizePlace.onDocumentFinalized -= DocumentFinalizePlaceOnDocumentFinalized;
        }

        private int CompareDocsRequirement(DocumentData data)
        {
            int missesMask = Mathf.Abs((int)(data.done ^ data.toDo));
            Debug.Log($"{data.toDo} {data.done} {missesMask}");
            int misses = 0;

            while (missesMask != 0)
            {
                misses += missesMask % 2;
                missesMask /= 2;
            }
            return misses;
        }

        public void AddDocuments(DocumentData[] documents)
        {
            documentsStack.AddToStack(documents);
        }
    }
}