using UnityEngine;

namespace Documents
{
    [CreateAssetMenu(fileName = "Rule", menuName = "Syb/Stack")]
    public class SimpleRule: Rule
    {
        public Color color;
        public DocumentRequirement requirement;
    }
}