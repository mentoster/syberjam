using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Documents
{
    public class DocumentsStack : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private TextMeshProUGUI documentsCountDisplay;
        private readonly Queue<DocumentData> _documents = new Queue<DocumentData>();
        public event Action<DocumentData> onDocumentTaken;

        public void AddToStack(DocumentData[] documents)
        {
            foreach (var document in documents)
            {
                _documents.Enqueue(document);
            }

            DisplayDocumentsChange();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            ShowDocument();
        }

        public void ShowDocument()
        {

            if (_documents.Count == 0)
                return;

            onDocumentTaken?.Invoke(_documents.Dequeue());
            DisplayDocumentsChange();
        }
        private void DisplayDocumentsChange()
        {
            documentsCountDisplay.text = $"{_documents.Count}";
        }
    }
}