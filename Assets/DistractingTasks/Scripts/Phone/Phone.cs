using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

namespace DistractingTasks.Scripts
{
    public class Phone : MonoBehaviour, IPointerClickHandler
    {
        private int score;
        public TMP_Text textOutput;
        
        private int _callNumber;
        public RedirectButton techSupportButton;
        public RedirectButton officeButton;
        private RedirectButton hangUp;
        private bool _isCalling;
        private bool _isReceiving;
        public TMP_Text scoreOutput;
        public string[] callTexts;
        public int[] correctAnswers;
        private int currentCorrectAswer;
        
        private bool oddFrame;
        private void Start()
        {
            _callNumber = 0;
            hangUp = GetComponent<RedirectButton>();
            
            score = 50;
            scoreOutput.text = score.ToString();
            StartCoroutine(CallDelay());
        }

        private void Update()
        {
            /*oddFrame = !oddFrame;
            if (_isCalling)
            {
                if (oddFrame)
                {
                    transform.Rotate(Vector3.forward, 3f);
                }
                else
                {
                    transform.Rotate(Vector3.forward, -3f);
                }
            }
            else
            {
                transform.rotation = Quaternion.Euler(Vector3.zero);
            }*/
        }

        void UpdateScore(int d)
        {
            score += d;
            scoreOutput.text = score.ToString();
        }

        private void MakeCall()
        {
            _isCalling = true;
            _callNumber++;
            StartCoroutine(MissCallTimer());
            Debug.Log("calling");
        }

        private void RecieveCall()
        {
            _isCalling = false;
            _isReceiving = true;
            if (callTexts.Length == 0)
            {
                Debug.Log("no calltexts given");
                return;
            }
            techSupportButton.active = true;
            officeButton.active = true;
            int rand = Random.Range(0, 10000)%callTexts.Length;
            StartCoroutine(SlowTextPrint(callTexts[rand]));
            currentCorrectAswer = correctAnswers[rand];
        }

        private void MissCall()
        {
            _isCalling = false;
            HandleAnswer(-1);
        }

        public void HandleAnswer(int givenAnswer)
        {
            if (givenAnswer == currentCorrectAswer)
            {
                UpdateScore(5);
                Debug.Log("correct");
            }
            else
            {
                UpdateScore(-5);
                Debug.Log("no");
            }
            techSupportButton.active = false;
            officeButton.active = false;
            _isReceiving = false;
            textOutput.text = "";
            StartCoroutine(CallDelay());
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (_isReceiving)
            {
                HandleAnswer(0);
                return;
            }
            if (_isCalling)
            {
                RecieveCall();
            }

            
        }

        private IEnumerator CallDelay()
        {
            yield return new WaitForSeconds(6f);
            MakeCall();
        }

        private IEnumerator SlowTextPrint( string text, int i = 0)
        {
            yield return new WaitForSeconds(0.05f);
            textOutput.text = textOutput.text + text[i];
            if(i != text.Length -1)
            {
                StartCoroutine(SlowTextPrint(text, i+1));
            }

            if (!_isReceiving)
            {
                textOutput.text = "";
            }
        }

        private IEnumerator MissCallTimer()
        {
            int callToMiss = _callNumber;
            yield return new WaitForSeconds(7f);
            if (_isCalling && callToMiss == _callNumber)
            {
                MissCall();
            }
        }
    }
}
