using UnityEngine;
using UnityEngine.EventSystems;

namespace DistractingTasks.Scripts
{
    public class RedirectButton : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private int answerCode;
        public Phone phone;
        public bool active;
        public void OnPointerClick(PointerEventData eventData)
        {
            if (active)
            {
                phone.HandleAnswer(answerCode);
            }
        }
    }
}
