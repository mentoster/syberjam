using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Toaster : MonoBehaviour
{
    public GameObject whiteBread;
    public GameObject blackBread;

    private void Start()
    {
        InvokeRepeating("makeBread",10f, 9f);
    }

    void makeBread()
    {
        int type = Random.Range(0, 2);
        switch (type)
        {
            case 0:
                Instantiate(whiteBread, transform.position, Quaternion.identity);
                break;
            case 1:
                Instantiate(blackBread, transform.position, Quaternion.identity);
                break;
        }
    }
}
