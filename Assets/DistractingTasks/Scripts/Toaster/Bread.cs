using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Bread : MonoBehaviour, IPointerClickHandler, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    public int mode;
    private Vector2 startPos;
    private Rigidbody2D rb;
    [SerializeField] private float throwForce;
    [SerializeField] private float throwTorque;
    [SerializeField] private float breadOffset;
    [SerializeField] private float animSpeed;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        startPos = transform.position;
    }
    void Update()
    {
        if ((startPos - (Vector2)transform.position).magnitude < breadOffset)
        {
            transform.Translate(Vector3.up * (animSpeed * Time.deltaTime));
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        rb.isKinematic = false;
        if (startPos.x < transform.position.x)
        {
            rb.AddTorque(throwTorque);
            rb.AddForce((Vector2.right + Vector2.up) * throwForce);
        }
        else
        {
            rb.AddTorque(-throwTorque);
            rb.AddForce((Vector2.left + Vector2.up) * throwForce);
        }
    }
}
