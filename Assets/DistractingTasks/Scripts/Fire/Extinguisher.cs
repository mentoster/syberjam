using FMODUnity;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DistractingTasks.Scripts.Fire
{
    public class Extinguisher : MonoBehaviour, IPointerClickHandler
    {
        public bool equiped;
        private SpriteRenderer sr;
        private Color color;
        void Start()
        {
            sr = GetComponent<SpriteRenderer>();
            color = sr.color;
        }
    

        public void OnPointerClick(PointerEventData eventData)
        {
            Debug.Log("ext click");
            if (!equiped)
            {
                equiped = true;
                sr.color = new Color(1, 1, 1, 0);
            }
            else
            {
                equiped = false;
                sr.color = color;
            }
        }
    }
}
