using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireSpawner : MonoBehaviour
{
    [SerializeField] private Vector3[] firePoints;
    [SerializeField] private GameObject fire;
    public float minDelay;
    public float maxDelay;

    void Start()
    {
        StartCoroutine(FireDelay());
    }
    void MakeFire()
    {
        if (firePoints.Length > 0)
        {
            int rand;
            rand = Random.Range(0, firePoints.Length);
            Instantiate(fire, firePoints[rand], Quaternion.identity);
        }
    }

    IEnumerator FireDelay()
    {
        float rand;
        rand = Random.Range(minDelay, maxDelay);
        yield return new WaitForSeconds(rand);
        MakeFire();
        StartCoroutine(FireDelay());
    }
}
