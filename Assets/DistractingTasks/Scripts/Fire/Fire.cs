using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

namespace DistractingTasks.Scripts.Fire
{
    public class Fire : MonoBehaviour, IPointerClickHandler
    {
        private static int fireCount;
        [SerializeField] private GameObject fire;
        [SerializeField] private float offsetX;
        [SerializeField] private float offsetY;
        [SerializeField] private float minSize;
        [SerializeField] private float maxSize;
        [SerializeField] private bool createsNewFire;
        [SerializeField] private float creatingPoint;
        public static event Action OnFireStarted;
        public static event Action OnFireFinished;
        public static event Action<int> OnFireTick;
        [SerializeField] private float increaseSpeed;
        [SerializeField] private float decreaseSpeed;
        private Extinguisher _extinguisher;
        
        void Start()
        {
            InvokeRepeating("DoScoreDamage",1f,1f);
            fireCount++;
            if (OnFireStarted != null)
            {
                OnFireStarted();
            }
            _extinguisher = GameObject.Find("Extinguisher").GetComponent<Extinguisher>();
        }

        void DoScoreDamage()
        {
            if (OnFireTick != null)
            {
                OnFireTick(1);
            }
        }

        private void Update()
        {
            if (transform.localScale.magnitude > maxSize)
            {
                return;
            }
            transform.localScale += (Vector3.one * (increaseSpeed * Time.deltaTime));
            if (createsNewFire && transform.localScale.magnitude > creatingPoint)
            {
                createsNewFire = false;
                Instantiate(fire, transform.position + new Vector3( Random.Range(-offsetX, offsetX),Random.Range(-offsetY,offsetY),0f),
                    Quaternion.identity);
                Instantiate(fire, transform.position + new Vector3( Random.Range(-offsetX, offsetX),Random.Range(-offsetY,offsetY),0f),
                    Quaternion.identity);
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (_extinguisher.equiped)
            {
                transform.localScale -= (Vector3.one * decreaseSpeed);
                if (transform.localScale.x < minSize)
                {
                    fireCount--;
                    if (fireCount == 0)
                    {
                        if (OnFireFinished != null)
                        {
                            OnFireFinished();
                        }
                    }
                    Destroy(gameObject);
                }
                
            }
        }
    }
}
