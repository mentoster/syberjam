using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.UI.Scripts.Sprites
{
    public class FullscreenSprite : MonoBehaviour
    {
        SpriteRenderer _spriteRenderer;

        private void Start()
        {

            _spriteRenderer = GetComponent<SpriteRenderer>();
        }
        void Update()
        {
            var cameraHeight = Camera.main.orthographicSize * 2;
            var cameraSize = new Vector2(Camera.main.aspect * cameraHeight, cameraHeight);
            Vector2 spriteSize = _spriteRenderer.sprite.bounds.size;

            Vector2 scale = transform.localScale;
            if (cameraSize.x >= cameraSize.y)
            { // Landscape (or equal)
                scale *= cameraSize.x / spriteSize.x;
            }
            else
            { // Portrait
                scale *= cameraSize.y / spriteSize.y;
            }

            transform.position = Vector2.zero; // Optional
            transform.localScale = scale;
        }
    }
}