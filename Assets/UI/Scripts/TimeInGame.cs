using System.Collections;
using TMPro;
using UnityEngine;

namespace Assets.UI.Scripts
{
    [RequireComponent(typeof(TMP_Text))]
    public class TimeInGame : MonoBehaviour
    {
        TMP_Text _timeText;
        float _allTime = 0;
        int _timeSpeed = 2;
        private void Start()
        {
            _allTime = 10 * 60;
            _timeText = GetComponent<TMP_Text>();
            StartCoroutine(TimerTick());
        }


        IEnumerator TimerTick()
        {
            while (true)
            {
                var minutes = (int)_allTime;
                var hours = minutes / 60 % 24;
                _timeText.text = hours.ToString("00") + ":" + (minutes % 60).ToString("00");
                yield return new WaitForSeconds(1f / _timeSpeed);
                _allTime += 1f;
            }

        }
    }
}