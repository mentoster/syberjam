using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Assets.UI.Scripts
{
    public class RecordsManager : MonoBehaviour
    {
        [SerializeField] TMP_Text[] _records;
        private void Start()
        {
            AddNewRecord(3);
            RecordsToTexts();
        }
        public void AddNewRecord(int points)
        {
            var records = PlayerPrefs.GetString("Records", "");
            var recordsList = records.Split(';');
            var newRecords = new List<string>();
            var added = false;
            foreach (var record in recordsList)
            {
                if (record == "")
                {
                    continue;
                }

                var recordPoints = int.Parse(record.Split(':')[1]);
                if (points > recordPoints && !added)
                {
                    newRecords.Add($"{PlayerPrefs.GetString("PlayerName", "Player")}:{points}");
                    added = true;
                }

                newRecords.Add(record);
            }

            if (!added)
            {
                newRecords.Add($"{PlayerPrefs.GetString("PlayerName", "Player")}:{points}");
            }

            var newRecordsString = "";
            foreach (var record in newRecords)
            {
                newRecordsString += record + ";";
            }

            PlayerPrefs.SetString("Records", newRecordsString);
        }


        public string[] LoadTopFiveRecods()
        {
            var records = PlayerPrefs.GetString("Records", "");
            var recordsList = records.Split(';');
            var newRecords = new List<string>();
            foreach (var record in recordsList)
            {
                if (record == "")
                {
                    continue;
                }

                newRecords.Add(record);
            }

            newRecords.Sort((x, y) => int.Parse(y.Split(':')[1]).CompareTo(int.Parse(x.Split(':')[1])));
            var topFive = new List<string>();
            for (var i = 0; i < 5; i++)
            {
                if (i >= newRecords.Count)
                {
                    break;
                }

                topFive.Add(newRecords[i]);
            }

            return topFive.ToArray();
        }
        private void RecordsToTexts()
        {
            if (_records.Length != 0)
            {
                var records = LoadTopFiveRecods();
                for (var i = 0; i < records.Length; i++)
                {
                    _records[i].text = $"{i + 1}. {records[i].Replace("Player:", "")}";
                }
            }

        }
    }

}