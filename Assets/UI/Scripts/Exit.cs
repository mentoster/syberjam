using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.UI.Scripts
{
    public class Exit : MonoBehaviour
    {
        public void ExitGame()
        {
                Application.Quit();
        }
    }
}