using System.Collections;
using System.Collections.Generic;
using Documents;
using TMPro;
using UnityEngine;

namespace Assets.UI.Scripts
{

    public class DialogPanel : MonoBehaviour
    {
        [SerializeField] TypingText _typingText;

        [SerializeField] TMP_Text _talkerName;
        [SerializeField] DialogData _startDialog;
        [SerializeField] GameObject _dialogSpriteTalker;
        [SerializeField] List<DialogData> _ezNuans;
        [SerializeField] List<DialogData> _meanNuans;
        [SerializeField] List<DialogData> _hardNuans;
        [SerializeField] GameObject _endScreen;
        [SerializeField] int _points = 0;
        [SerializeField] TMP_Text _pointsText;
        List<List<DialogData>> _allNuans;
        int _nuansLevel = 0;
        int _nuansPassed = 0;
        [SerializeField] TMP_Text _nuansPassedText;
        [SerializeField] int _nuansFailedToLose = 5;
        [SerializeField] TMP_Text _nuansFailedText;
        int _nuansPassedToNextLevel = 4;
        int _maxDocs = 4;
        private DocumentsGenerator _generator;
        private DocumentsController _controller;
        private DocumentsStack _stack;
        private RecordsManager _records;
        private TimerSlider _timerSlider;

        bool _pressToContinue;
        [SerializeField] GameObject _dialogObject;

        private void Start()
        {
            _generator = FindObjectOfType<DocumentsGenerator>();
            _controller = FindObjectOfType<DocumentsController>();
            _stack = FindObjectOfType<DocumentsStack>();
            _records = FindObjectOfType<RecordsManager>();
            _controller.onDocumentFinalized += ControllerOnDocumentFinalized;
            _timerSlider = FindObjectOfType<TimerSlider>();
            _timerSlider.timeEnds += TimeEnds;
            ShowDialog(_startDialog);
            _allNuans = new List<List<DialogData>>();
            _allNuans.Add(_ezNuans);
            _allNuans.Add(_meanNuans);
            _allNuans.Add(_hardNuans);
            _nuansFailedText.text = _nuansFailedToLose.ToString();
        }
        private void Update()
        {

            if (!_pressToContinue)
            {
                return;
            }

            if (_pressToContinue && (Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButtonDown(0)))
            {
                HideDialog();
            }
        }
        private void OnDestroy()
        {
            _controller.onDocumentFinalized -= ControllerOnDocumentFinalized;
            _timerSlider.timeEnds -= TimeEnds;
        }
        public void GeneratesDialog()
        {
            if (_allNuans[_nuansLevel].Count != 0)
            {
                _dialogObject.SetActive(value: true);
                var nuans = Random.Range(0, _ezNuans.Count);
                var dialogData = _allNuans[_nuansLevel][nuans];
                _dialogSpriteTalker.SetActive(value: true);
                ShowDialog(dialogData);
                _allNuans[0].RemoveAt(nuans);
                _timerSlider.IsGo = false;
            }
            else
            {
                if (++_nuansLevel != _allNuans.Count)
                {
                    GeneratesDialog();
                }
            }
        }
        void HideDialog()
        {
            _pressToContinue = false;
            _dialogObject.SetActive(false);
            _dialogSpriteTalker.SetActive(value: false);
            _generator.rules.Add(_startDialog.simpleRule);
            _maxDocs += 2;
            _nuansPassedToNextLevel = _maxDocs;
            _generator.FillWithDocuments(_nuansPassedToNextLevel);
            _timerSlider.IsGo = true;
            //Time.timeScale = 1
            _stack.ShowDocument();
        }
        public void ShowDialog(DialogData data, int index = 0)
        {
            if (index < data.say.Length)
            {
                _typingText.SetText(data, index, ShowDialog);
                _talkerName.text = data.talkerName;
            }
            else
            {
                _pressToContinue = true;
            }

        }
        private void ControllerOnDocumentFinalized(int score)
        {
            _timerSlider.CompletedTask();
            _nuansPassedToNextLevel--;
            if (_nuansPassedToNextLevel == 0)
            {
                GeneratesDialog();
            }
            if (score != 0)
            {
                Error();
                return;
            }
            _points++;

            _nuansPassed++;
            _nuansPassedText.text = $"{_nuansPassed} ���.";
            _stack.ShowDocument();
        }
        private void TimeEnds()
        {
            _timerSlider.CompletedTask();
            Error();
        }
        private void Error()
        {
            _nuansPassed++;
            FMODUnity.RuntimeManager.PlayOneShot("event:/error");
            _nuansFailedToLose--;
            _nuansFailedText.text = _nuansFailedToLose.ToString();
            if (_nuansFailedToLose <= 0)
            {
                Fired();
            }
        }

        private void Fired()
        {
            _endScreen.SetActive(true);
            _records.AddNewRecord(_points);
            _pointsText.text = $"{_points} ���.";
            _timerSlider.IsGo = false;
        }
    }
}