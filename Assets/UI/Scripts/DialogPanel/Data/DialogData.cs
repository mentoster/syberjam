using System.Collections;
using System.Collections.Generic;
using Documents;
using UnityEngine;

namespace Assets.UI.Scripts
{
    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/DialogData")]
    public class DialogData : ScriptableObject
    {
        [TextArea(3, 10)]
        public string[] say;
        public string talkerName;
        public SimpleRule simpleRule;
    }
}