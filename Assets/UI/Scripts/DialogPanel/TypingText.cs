using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.UI.Scripts
{
    [RequireComponent(typeof(TMP_Text))]
    public class TypingText : MonoBehaviour
    {

        TMP_Text _text;
        [SerializeField] float _time;
        [SerializeField] float _panelTime = 3f;
        DialogData _lastData;
        int _lastIndex = 0;
        UnityAction<DialogData, int> _lastAction;
        [SerializeField] GameObject _plzPressButtonText;
        [SerializeField] Animator _talkerAnimator;
        bool _canPress = false;
        private void Awake()
        {
            _text = GetComponent<TMP_Text>();
        }

        private void OnEnable()
        {
            StartCoroutine(WaitBeforeCanSkip());
        }
        private void OnDisable()
        {
            _canPress = false;
        }
        private void Update()
        {
            if (!_canPress)
            {
                return;
            }

            if (Input.GetMouseButtonDown(0))
            {
                StopAllCoroutines();
                if (_lastIndex >= _lastData.say.Length)
                {
                    return;
                }

                _text.text = _lastData.say[_lastIndex];
                _plzPressButtonText.SetActive(true);
                _lastAction?.Invoke(_lastData, ++_lastIndex);
                _talkerAnimator.SetBool("Moving", false);
            }
        }
        IEnumerator WaitBeforeCanSkip()
        {
            yield return new WaitForSeconds(_panelTime);
            _canPress = true;
        }
        private IEnumerator AnimateText(DialogData text, int index, UnityAction<DialogData, int> nextPhrase)
        {
            var newText = DeleteRichTags(text.say[index]);
            _talkerAnimator.SetBool("Moving", true);
            _plzPressButtonText.SetActive(false);
            _lastData = text;
            _lastIndex = index;
            _lastAction = nextPhrase;
            var i = 0;
            _text.text = "";
            while (i < newText.Length)
            {
                _text.text += newText[i++];
                if (_text.text != " ")
                {
                    FMODUnity.RuntimeManager.PlayOneShot("event:/key");
                }
                yield return new WaitForSeconds(_time);
            }
            _text.text = text.say[index];
            StartCoroutine(AnimateNextPhrase());
            _plzPressButtonText.SetActive(true);
            _talkerAnimator.SetBool("Moving", false);
        }
        private string DeleteRichTags(string text)
        {
            var result = "";
            var i = 0;
            while (i < text.Length)
            {
                if (text[i] == '<')
                {
                    while (text[i] != '>')
                    {
                        i++;
                    }
                }
                else
                {
                    result += text[i];
                }
                i++;
            }
            return result;
        }
        private IEnumerator AnimateNextPhrase()
        {
            yield return new WaitForSeconds(_panelTime);
            _lastAction?.Invoke(_lastData, ++_lastIndex);
        }

        public void SetText(DialogData text, int index, UnityAction<DialogData, int> nextPhrase)
        {
            StartCoroutine(AnimateText(text, index, nextPhrase));
        }
    }
}