using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.UI.Scripts
{
    [RequireComponent(typeof(RectTransform))]
    public class TimerSlider : MonoBehaviour
    {
        [SerializeField] float _canvasQuality = 1920;
        RectTransform _rect;
        float _procentNow = 0;
        float _secondToComplete = 30;
        public UnityAction timeEnds;
        public bool IsGo { get; set; }

        private void Start()
        {
            _rect = GetComponent<RectTransform>();
        }

        private void Update()
        {
            if (!IsGo)
            {
                return;
            }

            if (_procentNow < 100)
            {
                _procentNow += Time.deltaTime * 100 / _secondToComplete;
                SetProcent(_procentNow);
            }
            else
            {
                timeEnds?.Invoke();
            }
        }

        private void SetProcent(float procent)
        {
            _rect.offsetMax = new Vector2(-_canvasQuality * (1 - ((100 - procent) / 100)), _rect.offsetMax.y);
        }
        public void ChangeSecondsOnComplete(float seconds)
        {
            _secondToComplete = seconds;
        }
        public void CompletedTask()
        {
            _procentNow = 0;
        }
    }
}