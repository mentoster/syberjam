using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.UI.Scripts
{
    public class FiredScript : MonoBehaviour
    {
        private void OnEnable()
        {
            Fired();
        }
        public void Fired()
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/udarch");
        }
    }
}
